#include "pebble_os.h"
#include "pebble_app.h"
#include "pebble_fonts.h"

enum {
  CMD_KEY = 0x0, // TUPLE_INTEGER
};

enum {
  CMD_UP = 0x01,
  CMD_SELECT = 0x02,
  CMD_DOWN = 0x03
};

#define MY_UUID { 0x5E, 0xD5, 0xCD, 0xD6, 0x43, 0xA6, 0x45, 0x34, 0x91, 0x2C, 0xE5, 0xFC, 0x83, 0x8F, 0xF1, 0x70 }
PBL_APP_INFO(MY_UUID,
             "Pebble Cam", "Vlado Grancaric",
             1, 0, /* App version */
             RESOURCE_ID_IMAGE_MENU_ICON,
             APP_INFO_STANDARD_APP);

#define COLOR_FOREGROUND GColorWhite
#define COLOR_BACKGROUND GColorBlack

Window window;

TextLayer text_layer;
TextLayer status_layer;

BitmapLayer camera_icon_layer;
HeapBitmap cameraIconLarge;

//ActionBar layer icons
HeapBitmap snapIcon;

ActionBarLayer action_bar;

static bool callbacks_registered;
static AppMessageCallbacksNode app_callbacks;

// utility function for initializing a text layer
void init_text(TextLayer* textlayer, int x, int y, int width, int height, ResourceId font)
{
  text_layer_init(textlayer, GRect(x, y, width, height));
  text_layer_set_text_alignment(textlayer, GTextAlignmentLeft);
  text_layer_set_text_color(textlayer, COLOR_FOREGROUND);
  text_layer_set_background_color(textlayer, GColorClear);
  text_layer_set_font(textlayer, fonts_load_custom_font(resource_get_handle(font)));
}

static void my_out_sent_handler(DictionaryIterator *sent, void *context) {
  // outgoing message was delivered
  text_layer_set_text(&status_layer, "Message Delivered");
}

static void app_send_failed(DictionaryIterator* failed, AppMessageResult reason, void* context) {
  text_layer_set_text(&status_layer, "Failed");
}

static void app_received_msg(DictionaryIterator* received, void* context) {
  //vibes_short_pulse();
}

bool register_callbacks() {
  if (callbacks_registered) {
    if (app_message_deregister_callbacks(&app_callbacks) == APP_MSG_OK)
      callbacks_registered = false;
  }
  if (!callbacks_registered) {
    app_callbacks = (AppMessageCallbacksNode){
      .callbacks = {
        .out_sent = my_out_sent_handler,
        .out_failed = app_send_failed,
        .in_received = app_received_msg
      },
      .context = NULL
    };
    if (app_message_register_callbacks(&app_callbacks) == APP_MSG_OK) {
      callbacks_registered = true;
    }
  }
  return callbacks_registered;
}

static void send_cmd(uint8_t cmd) {
  Tuplet value = TupletInteger(CMD_KEY, cmd);
  
  DictionaryIterator *iter;
  app_message_out_get(&iter);
  
  if (iter == NULL)
  {
    return;
  }

  dict_write_tuplet(iter, &value);
  dict_write_end(iter);
  
  app_message_out_send();
  app_message_out_release();

  text_layer_set_text(&status_layer, "Sent Message");
}

void up_single_click_handler(ClickRecognizerRef recognizer, Window *window) {
  (void)recognizer;
  (void)window;
  
  send_cmd(CMD_UP);
  //vibes_short_pulse();
}

void select_single_click_handler(ClickRecognizerRef recognizer, Window *window) {
  (void)recognizer;
  (void)window;
  
  send_cmd(CMD_SELECT);
  vibes_short_pulse();
}

void down_single_click_handler(ClickRecognizerRef recognizer, Window *window) {
  (void)recognizer;
  (void)window;
  
  send_cmd(CMD_DOWN);
  //vibes_short_pulse();
}

void single_click_handler(ClickRecognizerRef recognizer, Window *window) {
  (void)recognizer;
  (void)window;
  
  switch(click_recognizer_get_button_id(recognizer)) {
    case BUTTON_ID_SELECT :
      send_cmd(CMD_SELECT);
      vibes_short_pulse();
      break;
    case BUTTON_ID_UP :
      send_cmd(CMD_UP);
      break;
    case BUTTON_ID_DOWN :
      send_cmd(CMD_DOWN);
      break;      
    default :
      break;
  }

  //send_cmd(CMD_DOWN);
  //vibes_short_pulse();
}

void config_provider(ClickConfig **config, Window *window) {
  (void)window;

  config[BUTTON_ID_SELECT]->click.handler = (ClickHandler) single_click_handler;
  config[BUTTON_ID_SELECT]->click.repeat_interval_ms = 100; // "hold-to-repeat" gets overridden if there's a long click handler configured!

  config[BUTTON_ID_UP]->click.handler = (ClickHandler) single_click_handler;
  config[BUTTON_ID_UP]->click.repeat_interval_ms = 100; // "hold-to-repeat" gets overriden if there's a long click handler configured!

  config[BUTTON_ID_DOWN]->click.handler = (ClickHandler) single_click_handler;
  config[BUTTON_ID_DOWN]->click.repeat_interval_ms = 100; // "hold-to-repeat" gets overriden if there's a long click handler configured!

}

void handle_init(AppContextRef ctx) {
  (void)ctx;

    //Init the current resources
  resource_init_current_app(&APP_RESOURCES);

  window_init(&window, "Pebble Cam");
  window_stack_push(&window, true /* Animated */);
  window_set_background_color(&window, COLOR_BACKGROUND); 
  
  init_text(&text_layer, 2, 2, 118, 34, RESOURCE_ID_FONT_ROBOTO_BOLD_22);
  init_text(&status_layer, 2, 36, 118, 34, RESOURCE_ID_FONT_ROBOTO_CONDENSED_SUBSET_10);

  text_layer_set_text(&text_layer, "Pebble Cam");
  text_layer_set_text(&status_layer, "Status Here");

  heap_bitmap_init(&cameraIconLarge, RESOURCE_ID_IMAGE_CAMERA_ICON_LARGE);
  bitmap_layer_init(&camera_icon_layer, GRect(2, 62, 103, 91));
  bitmap_layer_set_bitmap(&camera_icon_layer, &cameraIconLarge.bmp);

  layer_add_child(&window.layer, &text_layer.layer);   
  layer_add_child(&window.layer, &status_layer.layer);
  layer_add_child(&window.layer, &camera_icon_layer.layer);

  register_callbacks();
  //window_set_click_config_provider(&window, (ClickConfigProvider) config_provider);

  // Initialize the action bar:
  action_bar_layer_init(&action_bar);
  action_bar_layer_set_background_color(&action_bar,COLOR_FOREGROUND);
  // Associate the action bar with the window:
  action_bar_layer_add_to_window(&action_bar, &window);
  // Set the click config provider:
  action_bar_layer_set_click_config_provider(&action_bar,
                                             (ClickConfigProvider) config_provider);
  // Set the icons:
  // The loading the icons is omitted for brevity... See HeapBitmap.
  heap_bitmap_init(&snapIcon, RESOURCE_ID_IMAGE_SNAP_ICON);


  //action_bar_layer_set_icon(&action_bar, BUTTON_ID_UP, &my_icon_previous);
  action_bar_layer_set_icon(&action_bar, BUTTON_ID_SELECT, &snapIcon.bmp);  
  //action_bar_layer_set_icon(&action_bar, BUTTON_ID_DOWN, &my_icon_next);  

  heap_bitmap_deinit(&snapIcon);
}


void pbl_main(void *params) {
  PebbleAppHandlers handlers = {
    .init_handler = &handle_init,
    .messaging_info = {
      .buffer_sizes = {
        .inbound = 64, //inbound buffer size in bytes
        .outbound = 16, //outbound buffer size in bytes
      }
    }    
  };
  app_event_loop(params, &handlers);
}

// Deinitialize resources on window unload that were initialized on window load
void window_unload(Window *me) {
  // Cleanup the menu icon
  heap_bitmap_deinit(&snapIcon);
  heap_bitmap_deinit(&cameraIconLarge);
}