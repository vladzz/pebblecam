# PebbleCam

This project is to create a sample app which can be installed on a iPhone to take photo's and do basic camera actions.
It will also have an accompanying Pebble App which can remotely control the app to zoom in/out and also take the photo.

Thank you to Sebastian Rehnby as his blog post iOS project structure was used for my project sturucture:

[Link to iOS project structure blog post](http://www.sebastianrehnby.com/blog/2013/01/15/structuring-an-ios-project/)

# Before opening project for the first time

This project uses CocoaPods so you need to run:

*	pod install

Then open 

*	PebbleCam.xcworkspace

that way you can pick up the cocoapod dependencies.