//
//  PCCameraView.h
//  PebbleCam
//
//  Created by Vlado Grancaric on 2/06/13.
//  Copyright (c) 2013 Vlado Grancaric. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FUIButton.h"
#import "PCCaptureSessionManager.h"

@interface PCCameraView : UIView

@property(nonatomic,strong) UILabel *label;
@property(nonatomic,strong) FUIButton *rotateCameraBtn;
@property(nonatomic,strong) FUIButton *snapBtn;

@property (strong) PCCaptureSessionManager *captureManager;

@end
