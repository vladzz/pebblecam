//
//  PCCaptureSessionManager.h
//  PebbleCam
//
//  Created by Vlado Grancaric on 2/06/13.
//  Copyright (c) 2013 Vlado Grancaric. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

#define kImageCapturedSuccessfully @"imageCapturedSuccessfully"

@interface PCCaptureSessionManager : NSObject {
}

@property (strong) AVCaptureVideoPreviewLayer *previewLayer;
@property (strong) AVCaptureSession *captureSession;
@property (strong) AVCaptureStillImageOutput *stillImageOutput;
@property (nonatomic, strong) UIImage *stillImage;

- (void)addVideoPreviewLayer;
- (void)addStillImageOutput;
- (void)captureStillImage;
- (void)addVideoInputFrontCamera:(BOOL)front;

@end