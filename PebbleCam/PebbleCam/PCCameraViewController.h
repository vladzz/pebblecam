//
//  PCCameraViewController.h
//  PebbleCam
//
//  Created by Vlado Grancaric on 2/06/13.
//  Copyright (c) 2013 Vlado Grancaric. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PCCameraView.h"

@interface PCCameraViewController : UIViewController

@property (strong) PCCameraView *cameraView;
@property BOOL isFrontCamera;

@end
