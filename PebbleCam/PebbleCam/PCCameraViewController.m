//
//  PCCameraViewController.m
//  PebbleCam
//
//  Created by Vlado Grancaric on 2/06/13.
//  Copyright (c) 2013 Vlado Grancaric. All rights reserved.
//

#import "PCCameraViewController.h"

#define kWatchButtonPressed @"0"
#define kWatchUpButton 1
#define kWatchSelectButton 2
#define kWatchDownButton 3

@interface PCCameraViewController () <PBPebbleCentralDelegate>
{
    id updateHandler;
}

@property(nonatomic,strong) PBWatch *targetWatch;
@property BOOL frontCamera;

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo;
@end

@implementation PCCameraViewController

-(void) loadView
{
    _cameraView = [[PCCameraView alloc] initWithFrame:CGRectZero];
    [self setView:_cameraView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
    [[_cameraView snapBtn] addTarget:self action:@selector(scanButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [[_cameraView rotateCameraBtn] addTarget:self action:@selector(switchCamera) forControlEvents:UIControlEventTouchUpInside];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveImageToPhotoAlbum) name:kImageCapturedSuccessfully object:nil];
    
    [self setIsFrontCamera:NO];
    
    [[[_cameraView captureManager] captureSession] setSessionPreset:@"AVCaptureSessionPresetPhoto"];
	[[[_cameraView captureManager] captureSession] startRunning];
    
    // We'd like to get called when Pebbles connect and disconnect, so become the delegate of PBPebbleCentral:
    [[PBPebbleCentral defaultCentral] setDelegate:self];
    
    // Initialize with the last connected watch:
    [self setTargetWatch:[[PBPebbleCentral defaultCentral] lastConnectedWatch]];
        
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateCaptureManager
{
    for (AVCaptureInput *i in [[[_cameraView captureManager] captureSession] inputs])
        [[[_cameraView captureManager] captureSession] removeInput:i];
    
    
    [[[_cameraView captureManager] captureSession] beginConfiguration];
	[[_cameraView captureManager] addVideoInputFrontCamera:_isFrontCamera];
    [[[_cameraView captureManager] captureSession] commitConfiguration];
}



- (void)switchCamera
{
    [self setIsFrontCamera:!_isFrontCamera];    //toggle camera between front and back
    [self updateCaptureManager];     //update the captureManager with the new camera view
}

- (void)scanButtonPressed {
	//[[self scanningLabel] setHidden:NO];
    [[_cameraView captureManager] captureStillImage];
}

- (void)flipButtonPressed {
	//[[self scanningLabel] setHidden:NO];
    _frontCamera = _frontCamera ? NO : YES;
    [[_cameraView captureManager] addVideoInputFrontCamera:_frontCamera];
}

- (void)saveImageToPhotoAlbum
{
    UIImageWriteToSavedPhotosAlbum([[_cameraView captureManager] stillImage], self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error != NULL) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!" message:@"Image couldn't be saved" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else {

    }
}

- (void)setTargetWatch:(PBWatch*)watch {
    _targetWatch = watch;
    
    // NOTE:
    // For demonstration purposes, we start communicating with the watch immediately upon connection,
    // because we are calling -appMessagesGetIsSupported: here, which implicitely opens the communication session.
    // Real world apps should communicate only if the user is actively using the app, because there
    // is one communication session that is shared between all 3rd party iOS apps.
    
    // Test if the Pebble's firmware supports AppMessages / Weather:
    [watch appMessagesGetIsSupported:^(PBWatch *watch, BOOL isAppMessagesSupported) {
        
        NSLog(@"Is App Messages Supported %i", isAppMessagesSupported);
        
        if (isAppMessagesSupported) {
            // Configure our communications channel to target the weather app:
            // See demos/feature_app_messages/weather.c in the native watch app SDK for the same definition on the watch's end:
            uint8_t bytes[] = {0x5E, 0xD5, 0xCD, 0xD6, 0x43, 0xA6, 0x45, 0x34, 0x91, 0x2C, 0xE5, 0xFC, 0x83, 0x8F, 0xF1, 0x70};
            NSData *uuid = [NSData dataWithBytes:bytes length:sizeof(bytes)];
            [watch appMessagesSetUUID:uuid];
            
            if(_targetWatch && updateHandler)
                [_targetWatch appMessagesRemoveUpdateHandler:updateHandler];
            
            updateHandler = [[self targetWatch] appMessagesAddReceiveUpdateHandler:^BOOL(PBWatch *watch, NSDictionary *update) {
                //Receive a Message from the Pebble App
                
                NSEnumerator *enumerator = [update keyEnumerator];
                
                NSInteger keyPressed = 0;
                
                for(NSString *aKey in enumerator) {
                    NSLog(@"Key pressed [%@]", aKey);
                    keyPressed = [[update objectForKey:aKey] integerValue];
                }
                
                
                //NSInteger keyPressed = [[update objectForKey:[@"0" intValue]] integerValue];
                
                NSLog(@"Button Pressed on Watch %i", keyPressed); //made up method
                
                switch (keyPressed) {
                    case kWatchUpButton:
                        [self flipButtonPressed];
                        break;
                    case kWatchSelectButton:
                        [self scanButtonPressed];
                    default:
                        break;
                }
                
                return YES;
            }];
            
            //NSString *message = [NSString stringWithFormat:@"Yay! %@ supports AppMessages :D", [watch name]];
            //[[[UIAlertView alloc] initWithTitle:@"Connected!" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            
            [[_cameraView label] setText:@"Connected"];
        } else {
            [[_cameraView label] setText:@"Watch not support AppMessages"];
            NSString *message = [NSString stringWithFormat:@"Blegh... %@ does NOT support AppMessages :'(", [watch name]];
            [[[UIAlertView alloc] initWithTitle:@"Connected..." message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
    }];
}

#pragma mark PBPebbleCentralDelegate Methods
- (void)pebbleCentral:(PBPebbleCentral*)central watchDidConnect:(PBWatch*)watch isNew:(BOOL)isNew {
    [self setTargetWatch:watch];
}

- (void)pebbleCentral:(PBPebbleCentral*)central watchDidDisconnect:(PBWatch*)watch {
    [[_cameraView label] setText:@"Not Connected"];
    //[[[UIAlertView alloc] initWithTitle:@"Disconnected!" message:[watch name] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    
    if(!_targetWatch)
        return;
    
    [_targetWatch closeSession:nil];
    
    if(updateHandler) {
        [_targetWatch appMessagesRemoveUpdateHandler:updateHandler];
        updateHandler = nil;
    }
    
    if (_targetWatch == watch || [watch isEqual:_targetWatch]) {
        [self setTargetWatch:nil];
    }
}

- (BOOL)shouldAutorotate {
    return NO;
}

-(void) dealloc
{
    if(_targetWatch)
        [_targetWatch closeSession:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self forKeyPath:kImageCapturedSuccessfully];
}
@end
