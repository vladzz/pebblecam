//
//  PCCameraView.m
//  PebbleCam
//
//  Created by Vlado Grancaric on 2/06/13.
//  Copyright (c) 2013 Vlado Grancaric. All rights reserved.
//

#import "PCCameraView.h"
#import "UITabBar+FlatUI.h"
#import "UIColor+FlatUI.h"
#import "UIFont+FlatUI.h"

@interface PCCameraView()
{
    UIDeviceOrientation currentOrientation;
}

- (void)rotateButtonOrientation:(id)sender;

@end

@implementation PCCameraView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _label = [[UILabel alloc] initWithFrame:CGRectZero];
        [_label setTextColor:[UIColor redColor]];
        
        _rotateCameraBtn = [FUIButton buttonWithType:UIButtonTypeCustom];
        [_rotateCameraBtn setTitle:@"Rotate" forState:UIControlStateNormal];
        _rotateCameraBtn.buttonColor = [UIColor turquoiseColor];
        _rotateCameraBtn.shadowColor = [UIColor greenSeaColor];
        _rotateCameraBtn.shadowHeight = 3.0f;
        _rotateCameraBtn.cornerRadius = 6.0f;
        [_rotateCameraBtn setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin];
        
        _snapBtn = [FUIButton buttonWithType:UIButtonTypeCustom];
        [_snapBtn setImage:[UIImage imageNamed:@"camera"] forState:UIControlStateNormal];
        _snapBtn.buttonColor = [UIColor turquoiseColor];
        _snapBtn.shadowColor = [UIColor greenSeaColor];
        _snapBtn.shadowHeight = 3.0f;
        _snapBtn.cornerRadius = 6.0f;
        [_rotateCameraBtn setAutoresizingMask:UIViewAutoresizingFlexibleBottomMargin];
        
        [self setCaptureManager:[[PCCaptureSessionManager alloc] init]];
        
        [[self captureManager] addVideoInputFrontCamera:NO]; // set to YES for Front Camera, No for Back camera
        
        [[self captureManager] addStillImageOutput];
        
        [[self captureManager] addVideoPreviewLayer];
        CGRect layerRect = [[self layer] bounds];
        [[[self captureManager] previewLayer] setBounds:layerRect];
        [[[self captureManager] previewLayer] setPosition:CGPointMake(CGRectGetMidX(layerRect),CGRectGetMidY(layerRect))];
        [[self layer] addSublayer:[[self captureManager] previewLayer]];
        
        [self addSubview:_label];
        [self addSubview:_snapBtn];
        [self addSubview:_rotateCameraBtn];
        
        [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
        [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(deviceOrientationDidChange:) name: UIDeviceOrientationDidChangeNotification object: nil];
        
    }
    return self;
}

-(void) layoutSubviews
{
    //CGRect newFrame = self.bounds;
    [_label setFrame:CGRectMake(10., 10., 150., 24.)];
    [_rotateCameraBtn setFrame:CGRectMake(self.bounds.size.width - 100., 10., 100., 24.)];
    [_snapBtn setFrame:CGRectMake(self.bounds.size.width/2-64., self.bounds.size.height - 44., 128., 44.)];
    
    CGRect layerRect = [[self layer] bounds];
    [[[self captureManager] previewLayer] setBounds:layerRect];
    [[[self captureManager] previewLayer] setPosition:CGPointMake(CGRectGetMidX(layerRect),CGRectGetMidY(layerRect))];
    
    //NSLog(@"layoutSubViews called");
    //NSLog(@"FrameSize: X: %f Y: %f Width: %f Height: %f", self.bounds.origin.x, self.bounds.origin.y, self.bounds.size.width, self.bounds.size.height);
    
    //CGRect newFrame = self.bounds;
    
}

- (void)deviceOrientationDidChange:(NSNotification *)notification {
    [self rotateButtonOrientation:self];
}

- (void)rotateButtonOrientation:(id)sender
{
    
    [UIView animateWithDuration:0.2f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
        if (orientation == UIDeviceOrientationPortrait) {
            CGAffineTransform rotate = CGAffineTransformMakeRotation (0.0);
            _snapBtn.transform = rotate;
        } else if (orientation == UIDeviceOrientationPortraitUpsideDown) {
            CGAffineTransform rotate = CGAffineTransformMakeRotation (M_PI * 180 / 180.0f);
            _snapBtn.transform = rotate;
        } else if (orientation == UIDeviceOrientationLandscapeLeft) {
            CGAffineTransform rotate = CGAffineTransformMakeRotation (M_PI * 90 / 180.0f);
            _snapBtn.transform = rotate;
        } else if (orientation == UIDeviceOrientationLandscapeRight) {
            CGAffineTransform rotate = CGAffineTransformMakeRotation (M_PI * 270 / 180.0f);
            _snapBtn.transform = rotate;
        }
    } completion:nil];
}

@end
