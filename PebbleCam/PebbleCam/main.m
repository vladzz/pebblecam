//
//  main.m
//  PebbleCam
//
//  Created by Vlado Grancaric on 2/06/13.
//  Copyright (c) 2013 Vlado Grancaric. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PCAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PCAppDelegate class]));
    }
}
