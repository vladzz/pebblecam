//
//  PCAppDelegate.h
//  PebbleCam
//
//  Created by Vlado Grancaric on 2/06/13.
//  Copyright (c) 2013 Vlado Grancaric. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PCCameraViewController.h"

@interface PCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) PCCameraViewController *viewController;
@end
